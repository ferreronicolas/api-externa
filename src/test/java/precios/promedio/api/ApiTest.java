package precios.promedio.api;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.AfterClass;
import org.junit.Test;

public class ApiTest {

    static Api api = new Api();
    
    @Test
    public void getNullTest() {
        assertEquals(api.getInfo(), api.get(""));
        assertEquals(api.getInfo(), api.get(null));
    }

    @Test
    public void getSeparadorTest() {
        assertEquals(":", api.get("separador"));
    }

    @Test
    public void getVersionTest() {
        assertEquals(api.getVersion(), api.get("version"));
    }

    @Test
    public void mensajeInvalidoTest() {
        assertEquals(api.invalido(), api.get("asdfj"));
    }
    
    @Test
    public void mensajeLargoTest() {
        assertEquals(api.invalido(), api.get("a,s,d,d"));
    }

    @Test
    public void mensajeAlojamientoIncorrectoTest() {
        assertEquals(api.invalido(), api.get("alojamiento:buenos aires"));
    }
    
    @Test
    public void mensajeAtraccionIncorrectoTest() {
        assertEquals(api.invalido(), api.get("atraccion:buenos aires"));
    }
    
    @Test
    public void ciudadExistenteTest() {
        api.agregarCiudad("buenos aires");
        String precioAlojamiento = api.getPrecioPromedioAlojamientos("buenos aires");
        String precioAtraccion   = api.getPrecioPromedioAtracciones("buenos aires");
        System.out.println(precioAlojamiento);
        System.out.println(precioAtraccion);
        assertEquals(precioAlojamiento, api.get("ALOJAMIENTO:buenos aires"));
        assertEquals(precioAtraccion, api.get("ATRACCION:buenos aires"));
    }

    @Test
    public void ciudadesInexistentesTest() {
        String ciudad1 = "ciudad 1";
        String precioAlojamiento = api.get("ALOJAMIENTO:"+ciudad1);
        String precioAtraccion   = api.get("ATRACCION:"+ciudad1);
        assertEquals(precioAlojamiento, api.getPrecioPromedioAlojamientos(ciudad1));
        assertEquals(precioAtraccion, api.getPrecioPromedioAtracciones(ciudad1));
        String ciudad2 = "ciudad 2";
        String precioAtraccion2   = api.get("ATRACCION:"+ciudad2);
        String precioAlojamiento2 = api.get("ALOJAMIENTO:"+ciudad2);
        assertEquals(precioAlojamiento2, api.getPrecioPromedioAlojamientos(ciudad2));
        assertEquals(precioAtraccion2, api.getPrecioPromedioAtracciones(ciudad2));
    }

    @AfterClass
    public static void sesionTest() {
        String sesion = api.get("sesion");
        String[] ciudades = sesion.split(api.getSeparador());
        assertEquals(3, ciudades.length);
        Arrays.stream(ciudades).forEach(System.out::println);
    }
}
