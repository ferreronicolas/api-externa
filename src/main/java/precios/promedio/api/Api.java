package precios.promedio.api;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Api {

    private static final String INFO = "API para obtener precios promedios de alojamientos y atracciones";
    private static final String VERSION = "1.0";
    private static final String SEPARADOR = ":";
    private static final String MSG_INVALIDO = "consulta inválida";
    private Map<String, String> alojamientos;
    private Map<String, String> atracciones;

    public Api() {
        alojamientos = new HashMap<String, String>();
        atracciones  = new HashMap<String, String>();
    }
    
    public String get(String consulta) {
        if (consulta == null || consulta.equals(""))
            return getInfo();
        if (consulta.equals("version"))
            return getVersion();
        if (consulta.equals("separador"))
            return getSeparador();
        if (consulta.equals("sesion"))
            return getCiudadesConsultadasEnSesion();
        String[] tokens = consulta.split(SEPARADOR);
        if (tokens.length != 2)
            return invalido();
        if (tokens[0].equals("ALOJAMIENTO"))
            return getPrecioPromedioAlojamientos(tokens[1]);
        if (tokens[0].equals("ATRACCION"))
            return getPrecioPromedioAtracciones(tokens[1]);
        return invalido();
    }

    String getSeparador() {
        return SEPARADOR;
    }

    String getVersion() {
        return VERSION;
    }
    String getInfo() {
        return INFO;
    }

    String getCiudadesConsultadasEnSesion() {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = alojamientos.keySet().iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(SEPARADOR);
            }
        }
        return sb.toString();
    }

    String getPrecioPromedioAlojamientos(String ciudad) {
        if (alojamientos.containsKey(ciudad))
            return alojamientos.get(ciudad);
        agregarCiudad(ciudad);
        return alojamientos.get(ciudad);
    }

    String getPrecioPromedioAtracciones(String ciudad) {
        if (atracciones.containsKey(ciudad))
            return atracciones.get(ciudad);
        agregarCiudad(ciudad);
        return atracciones.get(ciudad);
    }

    void agregarCiudad(String ciudad) {
        alojamientos.put(ciudad, String.valueOf(Math.random() * 100));
        atracciones.put(ciudad, String.valueOf(Math.random() * 100));
    }

    String invalido() {
        return MSG_INVALIDO;
    }
}
